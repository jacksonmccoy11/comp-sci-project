package unl.cse;

public class Person {

    private String personCode;
    private String firstName;
    private String lastName;
    private Address address;
    private String[] emails;

    public Person(String personCode, String firstName, String lastName, Address address, String[] emails) {
        this.personCode = personCode;
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
        this.emails = emails;
    }

    public String getPersonCode() {
        return this.personCode;
    }

    public String getLastName() {
        return this.lastName;
    }

    public String getFirstName() {
        return this.firstName;
    }

    public Address getAddress() {
        return this.address;
    }

    public String[] getEmailAddress() {
        return this.emails;
    }
}
