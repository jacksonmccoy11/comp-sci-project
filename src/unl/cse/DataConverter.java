package unl.cse;

        import com.ceg.ext.DatabaseData;
        import unl.cse.Products.*;

        import java.util.ArrayList;

public class DataConverter {
    public static void main(String args[]) {
        DatabaseData db = new DatabaseData();

        ArrayList<Person> persons = db.getPersons();
        ArrayList<Customer> customers = db.getCustomers(persons);
        ArrayList<Product> products = db.getProductsFromDb();
        ArrayList<Invoice> invoices = db.getInvoices(persons, customers, products);

        PrintInvoice print = new PrintInvoice();

        print.PrintingInvoice(invoices);
    }
}
