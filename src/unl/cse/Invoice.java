package unl.cse;

import unl.cse.Products.Product;

import java.util.ArrayList;

public abstract class Invoice {
    private String invoiceCode;
    private Customer customer;
    private Person person;
    private String date;
    private ArrayList<Product> products;
    private int[] numOfProductsPurchased;

    public Invoice(String invoiceCode, Customer customer, Person person, String date, ArrayList<Product> products, int[] numOfProductsPurchased) {
        this.invoiceCode = invoiceCode;
        this.customer = customer;
        this.person = person;
        this.date = date;
        this.products = products;
        this.numOfProductsPurchased = numOfProductsPurchased;
    }

    public String getInvoiceCode() {
        return this.invoiceCode;
    }

    public Customer getCustomer() {
        return this.customer;
    }

    public Person getPerson() {
        return this.person;
    }

    public ArrayList<Product> getProduct() {
        return this.products;
    }

    public String getDate() {
        return this.date;
    }

    //Gets subtotal for all the products the customer purchased
    public double getSubtotal() {
        double subtotal = 0;
        int index = 0;

        for(Product p: this.products) {
            subtotal += p.getCost() * numOfProductsPurchased[index];
            index++;
        }

        return subtotal;
    }

    //Gets the discount percentage of all the products
    public double getDiscount(String type) {
        if(type.equals("S")) {
            return .1 * -this.getSubtotal();
        } else { return 0; }
    }

    //Gets the tax for all the products the customer purchased
    public double getTax() {
        return .037 * this.getSubtotal();
    }

    public double getFee(String type) {
        if(type.equals("S")) { return 6.75; }
        else { return 0; }
    }

    //Gets subtotal for a single product type
    public abstract double getSubtotal(Product product);

    public abstract int getNumOfProducts(Product product);

    //Gets the tax for a single product type
    public abstract double getTax(Product product);

    //Gets the discount for a single product type
    public abstract double getDiscount(String type, Product product);

    public abstract String getLinkedProductCode(Product product);
}
