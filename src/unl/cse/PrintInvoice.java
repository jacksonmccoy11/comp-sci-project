package unl.cse;

import unl.cse.Products.*;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

public class PrintInvoice {

    public void PrintingInvoice(ArrayList<Invoice> invoices) {
        PrintWriter print = null;

        try {
            print = new PrintWriter( new FileWriter("src/data/output.txt"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        print.println("Executive Summary Report\n" + "=========================" );
        print.printf("%-10s %-40s %-20s %13s %11s %11s %11s %11s\n",
                "unl.cse.Invoice", "unl.cse.Customer", "Salesperson", "Subtotal", "Fees", "Taxes", "Discount", "Total");

        double totalSubs = 0;
        double totalFees = 0;
        double totalTaxes = 0;
        double totalDiscounts = 0;
        double totals = 0;

        for(Invoice i: invoices) {
            String customerType = i.getCustomer().getType();
            String type = "";

            double subtotal = i.getSubtotal();
            double tax = i.getTax();
            double fee = i.getFee(customerType);
            double discount = i.getDiscount(customerType);
            double total = subtotal + fee + tax + discount;

            totalSubs += subtotal;
            totalFees += fee;
            totalTaxes += tax;
            totalDiscounts += discount;
            totals += total;

            switch(customerType) {
                case "G":
                    type = " [General]";
                    break;
                case "S":
                    type = " [Student]";
                    break;
            }

            String customer = i.getCustomer().getName() + type;
            String salesPerson = i.getPerson().getLastName() + ", " + i.getPerson().getFirstName();

            print.printf("%-10s %-40s %-21s $%11.2f $%10.2f $%10.2f $%10.2f $%10.2f\n",
                    i.getInvoiceCode(), customer, salesPerson, subtotal, fee, tax, discount, total);
        }
        print.println("======================================================================================================================================");

        print.printf("%-73s $%11.2f $%10.2f $%10.2f $%10.2f $%10.2f\n\n\n", "TOTALS", totalSubs, totalFees, totalTaxes, totalDiscounts, totals);
        print.println("Individual unl.cse.Invoice Detail Reports");
        print.println("=================================");

        for(Invoice i: invoices) {
            print.println("unl.cse.Invoice " + i.getInvoiceCode());
            print.println("===============================");

            print.println("Salesperson: " + i.getPerson().getLastName() + "," + i.getPerson().getFirstName());
            print.println("unl.cse.Customer Info:");
            print.println("  " + i.getCustomer().getName());
            switch(i.getCustomer().getType()) {
                case "G":
                    print.println("  [General]");
                    break;
                case "S":
                    print.println("  [Student]");
                    break;
            }
            Person primaryContact = i.getCustomer().getPrimaryContact();
            print.println("  " + primaryContact.getLastName() + "," + primaryContact.getFirstName());

            Address address = i.getCustomer().getAddress();
            print.println("  " + address.getStreet());
            print.println("  " + address.getCity() + " " + address.getState() + " " + address.getZip() + " " + address.getCountry());

            print.println("--------------------------------");

            print.printf("%-10s %-100s %12s %11s %11s\n", "Code", "Item", "SubTotal", "Tax", "Total");

            this.printIndividualReports(i, print);
        }
        print.close();
    }

    public void printIndividualReports(Invoice i, PrintWriter print) {

        int index = 0;
        ArrayList<Product> products = i.getProduct();
        for (Product product : products) {

            String type = "";
            String extraLength = "";

            switch (product.getType()) {
                case "M":
                    MovieTickets m = (MovieTickets) product;

                    type = "MovieTicket " + m.getName() + " @ " + m.getAddress();
                    extraLength = m.getDateTime() + " (" + i.getNumOfProducts(product) + " units @ $" + m.getCost() + "/unit)";
                    break;
                case "P":
                    Parking p = (Parking) product;

                    type = "ParkingPass " + i.getLinkedProductCode(product) + " (" + i.getNumOfProducts(product) + " units @ $" + p.getCost() + "/unit)";
                    break;
                case "R":
                    Refreshments r = (Refreshments) product;

                    type = r.getName() + " (" + i.getNumOfProducts(product) + " units @ $" + r.getCost() + "/unit)";
                    break;
                case "S":
                    Season s = (Season) product;

                    type = "Season Pass - " + s.getName() + " (" + i.getNumOfProducts(product) + " units @ $" + s.getCost() + "/unit)";
                    break;
            }
            double subtotal = i.getSubtotal(product);
            double tax = i.getTax(product);

            if (product.getType().equals("M")) {


                print.printf("%-10s %-100s $%11.2f $%10.2f $%10.2f\n",
                        product.getProductCode(), type, subtotal, tax, subtotal + tax);
                print.printf("%-10s %-100s\n",
                        "", extraLength);

            } else {
                print.printf("%-10s %-100s $%11.2f $%10.2f $%10.2f\n",
                        product.getProductCode(), type, subtotal, tax, subtotal + tax);
            }
        }
        print.printf("%148s\n", "====================================");
        print.printf("%-111s $%11.2f $%10.2f $%10.2f\n",
                "SUB-TOTALS", i.getSubtotal(), i.getTax(), i.getSubtotal() + i.getTax());

        switch(i.getCustomer().getType()) {
            case "S":
                print.printf("%-136s $%10.2f\n",
                        "DISCOUNT (10% STUDENT)", i.getDiscount("S"));
                print.printf("%-136s $%10.2f\n",
                        "FEE (Student)", i.getFee("S"));
                print.printf("%-136s $%10.2f\n",
                        "TOTAL", i.getSubtotal() + i.getTax() + i.getDiscount("S") + i.getFee("S"));
                break;
            case "G":
                print.printf("%-136s $%10.2f\n",
                        "TOTAL", i.getSubtotal() + i.getTax());
                break;
        }

        print.print("\n\n       Thank you for your purchase!\n\n");
    }
}
