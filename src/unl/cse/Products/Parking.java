package unl.cse.Products;

public class Parking extends Product {
    private double parkingFee;

    public Parking(String productCode, double parkingFee) {
        super(productCode);
        this.parkingFee = parkingFee;
    }

    public String getType() { return "P"; }
    public double getCost() { return parkingFee; }
}