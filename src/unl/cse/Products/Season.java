package unl.cse.Products;


public class Season extends Product {
    private String name;
    private String startDate;
    private String endDate;
    private double cost;

    public Season(String productCode, String name, String startDate, String endDate, double cost) {
        super(productCode);
        this.name = name;
        this.startDate = startDate;
        this.endDate = endDate;
        this.cost = cost;
    }

    public String getName() {
        return this.name;
    }

    public String getStartDate() {
        return this.startDate;
    }

    public String getEndDate() {
        return this.endDate;
    }

    public double getCost() {
        return this.cost;
    }

    public String getType() {
        return "S";
    }
}
