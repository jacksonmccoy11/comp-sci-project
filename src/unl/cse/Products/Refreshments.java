package unl.cse.Products;

public class Refreshments extends Product {
    private String name;
    private double cost;

    public Refreshments(String productCode, String name, double cost) {
        super(productCode);
        this.name = name;
        this.cost = cost;
    }

    public double getCost() {
        return this.cost;
    }

    public String getName() {
        return this.name;
    }

    public String getType() { return "R"; }
}
