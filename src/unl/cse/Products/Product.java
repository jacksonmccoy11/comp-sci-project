package unl.cse.Products;

public abstract class Product {

    private String productCode;

    public Product(String productCode) {
        this.productCode = productCode;
    }

    public String getProductCode() {
        return this.productCode;
    }

    public abstract String getType();

    public abstract double getCost();
}
