package unl.cse.Products;

public class MovieTickets extends Product {

    private String dateTime;
    private String movieName;
    private String address;
    private String screenNo;
    private double pricePerUnit;

    public MovieTickets(String productCode, String dateTime, String movieName, String address, String screenNo, double pricePerUnit) {
        super(productCode);
        this.dateTime = dateTime;
        this.movieName = movieName;
        this.address = address;
        this.screenNo = screenNo;
        this.pricePerUnit = pricePerUnit;
    }

    public String getDateTime() {
        return this.dateTime;
    }

    public String getName() {
        return this.movieName;
    }

    public String getScreenNo() {
        return this.screenNo;
    }

    public double getCost() { return pricePerUnit; }

    public String getType() {
        return "M";
    }

    public String getAddress() {
        return this.address;
    }
}