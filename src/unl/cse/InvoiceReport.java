package unl.cse;

import unl.cse.Products.Product;

import java.util.ArrayList;

public class InvoiceReport extends Invoice {

    private ArrayList<Product> products;
    private String[] linkedProductCodes;
    private int[] numOfProducts;
    
    public InvoiceReport(String invoiceCode, Customer customer, Person person, String date, ArrayList<Product> products, int[] numOfProducts, String[] linkedProductCode) {
        super(invoiceCode, customer, person, date, products, numOfProducts);
        this.products = products;
        this.linkedProductCodes = linkedProductCode;
        this.numOfProducts = numOfProducts;
    }

    public ArrayList<Product> getProducts() {
        return this.products;
    }

    //Gets subtotal for a single product type
    public double getSubtotal(Product product) {
        int index = products.indexOf(product);
        double subtotal = product.getCost() * numOfProducts[index];

        return subtotal;
    }

    public int getNumOfProducts(Product product) {
        int index = products.indexOf(product);

        return this.numOfProducts[index];
    }

    public double getFee(String type) {
        if(type.equals("S")) { return 6.75; }
        else { return 0; }
    }

    //Gets the tax for a single product type
    public double getTax(Product product) {
        return .037 * this.getSubtotal(product);
    }

    //Gets the discount for a single product type
    public double getDiscount(String type, Product product) {
        if(type.equals("S")) {
            return .1 * -this.getSubtotal(product);
        } else { return 0; }
    }

    public String getLinkedProductCode(Product product) {
        int index = products.indexOf(product);

        return this.linkedProductCodes[index];
    }
}
