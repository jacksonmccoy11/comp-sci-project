package unl.cse;

import unl.cse.Products.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class FileConverter {
    private String[] linkedProductCodes;
    private int[] numOfProductsPurchased;

    public ArrayList<Invoice> convertInvoices(ArrayList<Invoice> invoices, ArrayList<Customer> customers, ArrayList<Person> people, ArrayList<Product> products) {
        //Scanner to scan file for data
        Scanner file = null;

        try {
            file = new Scanner(new File("src/data/Invoices.dat"));
        } catch(FileNotFoundException e) {
            System.out.println(e);
        }

        //Gets the number of products
        int fileLength = file.nextInt();
        //Skips to the second line to start parsing data
        file.nextLine();

        //Parses invoices and adds them to Array
        for(int i = 0; i < fileLength; i++) {
            String[] line = file.nextLine().split(";");

            String invoiceCode = line[0];
            String customer = line[1];
            String person = line[2];
            String date = line[3];
            String[] productStrings = line[4].split(",");

            InvoiceReport invoice = CreateInvoiceReport(customers, people, products, invoiceCode, customer, person, date, productStrings);
            invoices.add(invoice);
        }

        return invoices;
    }

    public InvoiceReport CreateInvoiceReport(ArrayList<Customer> customers, ArrayList<Person> people, ArrayList<Product> products, String invoiceCode, String customerCode, String personCode, String date, String[] productCodes) {

        //Finds customer using customer code
        Customer customer = null;
        for(Customer c: customers) {
            if(customerCode.equals(c.getCustomerCode())) {
                customer = c;
            }
        }

        //Finds person using person code
        Person person = null;
        for(Person p: people) {
            if(personCode.equals(p.getPersonCode())) {
                person = p;
            }
        }

        //Calls ShoppingCart to get the list of products
        ArrayList<Product> productsPurchased = this.ShoppingCart(products, productCodes);

        InvoiceReport invoiceReport = new InvoiceReport(invoiceCode, customer, person, date, productsPurchased, this.numOfProductsPurchased, this.linkedProductCodes);

        return invoiceReport;
    }

    //Finds all of the products that the customer purchased
    private ArrayList<Product> ShoppingCart(ArrayList<Product> products, String[] productCodes) {
        ArrayList<Product> productsPurchase = new ArrayList<Product>();
        linkedProductCodes = new String[productCodes.length];
        numOfProductsPurchased = new int[productCodes.length];

        //Loops through each product code
        for(int i = 0; i < productCodes.length; i++) {
            String[] line = productCodes[i].split(":");

            //Loops through each product to find the productCodes that match
            for(Product p: products) {
                if(line[0].equals(p.getProductCode())){
                    productsPurchase.add(p);
                    //If the product codes match, use the number to add the same product that many times to the list
                    this.numOfProductsPurchased[i] = Integer.parseInt(line[1]);

                    if(line.length > 2) {
                        linkedProductCodes[i] = line[2];
                    }
                }
            }
        }

        return productsPurchase;
    }
}
