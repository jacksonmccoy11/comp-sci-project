package unl.cse;

public class Customer {

    private String customerCode;
    private String type;
    private Person primaryContact;
    private String customerName;
    private Address customerAddress;

    public Customer(String customerCode, String type, Person primaryContact, String customerName, Address customerAddress) {
        this.customerCode = customerCode;
        this.type = type;
        this.primaryContact = primaryContact;
        this.customerName = customerName;
        this.customerAddress = customerAddress;
    }

    public String getCustomerCode() {
        return this.customerCode;
    }

    public String getType() {
        return this.type;
    }

    public Person getPrimaryContact() {
        return this.primaryContact;
    }

    public String getName() {
        return this.customerName;
    }

    public Address getAddress() {
        return this.customerAddress;
    }

}
