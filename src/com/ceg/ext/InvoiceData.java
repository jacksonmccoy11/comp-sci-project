package com.ceg.ext;

/*
 * This is a collection of utility methods that define a general API for
 * interacting with the database supporting this application.
 * 15 methods in total, add more if required.
 * Donot change any method signatures or the package name.
 * 
 */

import com.google.gson.internal.bind.SqlDateTypeAdapter;

import java.sql.*;

public class InvoiceData {
	public static Connection conn = DatabaseInfo.getConnection();

	/**
	 * 1. Method that removes every person record from the database
	 */
	public static void removeAllPersons() {
		try {
			Statement stmt = conn.createStatement();
			String sql = "DELETE FROM Persons";
			int rows = stmt.executeUpdate(sql);
		}
		catch (SQLException e) {
			System.out.println(e);
		}
	}

	/**
	 * 2. Method to add a person record to the database with the provided data.
	 * 
	 * @param personCode
	 * @param firstName
	 * @param lastName
	 * @param street
	 * @param city
	 * @param state
	 * @param zip
	 * @param country
	 */
	public static void addPerson(String personCode, String firstName, String lastName, String street, String city, String state, String zip, String country) {
		try {
			String sql = "SELECT * FROM Persons p WHERE p.PersonCode LIKE ?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();

			if(rs.next()) {
				try {
					throw new Exception("This person already exists in Persons");
				}
				catch(Exception e) {
					System.out.println(e);
				}
			}

			sql = "INSERT INTO Persons (PersonCode, FirstName, LastName, Street, City, State, Zip, Country) VALUES (?,?,?,?,?,?,?,?)";
			ps = conn.prepareStatement(sql);
			ps.setString(1, personCode);
			ps.setString(2, firstName);
			ps.setString(3, lastName);
			ps.setString(4, street);
			ps.setString(5, city);
			ps.setString(6, state);
			ps.setString(7, zip);
			ps.setString(8, country);
			ps.executeUpdate();
		}
		catch (SQLException e) {
			System.out.println(e);
		}
	}

	/**
	 * 3. Adds an email record corresponding person record corresponding to the
	 * provided <code>personCode</code>
	 * 
	 * @param personCode
	 * @param email
	 */
	public static void addEmail(String personCode, String email) {
		try {
			String sql = "SELECT * FROM Emails e WHERE e.PersonCode LIKE ? && e.Email LIKE ?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();

			if(rs.next()) {
				try {
					throw new Exception("This email already exists in Emails for this person");
				}
				catch(Exception e) {
					System.out.println(e);
				}
			}

			sql = "INSERT INTO Emails (PersonCode, Email) VALUES (?,?)";
			ps = conn.prepareStatement(sql);
			ps.setString(1, personCode);
			ps.setString(2, email);
			ps.executeUpdate();
		}
		catch(SQLException e) {
			System.out.println(e);
		}
	}

	/**
	 * 4. Method that removes every customer record from the database
	 */
	public static void removeAllCustomers() {
		try {
			Statement stmt = conn.createStatement();
			String sql = "DELETE FROM Customers";
			int rows = stmt.executeUpdate(sql);
		}
		catch (SQLException e) {
			System.out.println(e);
		}
	}

	public static void addCustomer(String customerCode, String customerType, String primaryContactPersonCode, String name, String street, String city, String state, String zip, String country) {
		try {
			String sql = "SELECT * FROM Persons p WHERE p.PersonCode LIKE ?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();

			if(rs.next()) {
				try {
					throw new Exception("This person already exists in Persons");
				}
				catch(Exception e) {
					System.out.println(e);
				}
			}

			sql = "INSERT INTO Customers (CustomerCode, CustomerType, SalesPersonCode, Name, Street, City, State, Zip, Country) VALUES (?,?,?,?,?,?,?,?)";
			ps = conn.prepareStatement(sql);
			ps.setString(1, personCode);
			ps.setString(2, firstName);
			ps.setString(3, lastName);
			ps.setString(4, street);
			ps.setString(5, city);
			ps.setString(6, state);
			ps.setString(7, zip);
			ps.setString(8, country);
			ps.executeUpdate();
		}
		catch (SQLException e) {
			System.out.println(e);
		}
	}
	
	/**
	 * 5. Removes all product records from the database
	 */
	public static void removeAllProducts() {}

	/**
	 * 6. Adds an movieTicket record to the database with the provided data.
	 */
	public static void addMovieTicket(String productCode, String dateTime, String movieName, String street, String city,String state, String zip, String country, String screenNo, double pricePerUnit) {}

	/**
	 * 7. Adds a seasonPass record to the database with the provided data.
	 */
	public static void addSeasonPass(String productCode, String name, String seasonStartDate, String seasonEndDate,	double cost) {}

	/**
	 * 8. Adds a ParkingPass record to the database with the provided data.
	 */
	public static void addParkingPass(String productCode, double parkingFee) {}

	/**
	 * 9. Adds a refreshment record to the database with the provided data.
	 */
	public static void addRefreshment(String productCode, String name, double cost) {}

	/**
	 * 10. Removes all invoice records from the database
	 */
	public static void removeAllInvoices() {}

	/**
	 * 11. Adds an invoice record to the database with the given data.
	 */
	public static void addInvoice(String invoiceCode, String customerCode, String salesPersonCode, String invoiceDate) {}

	/**
	 * 12. Adds a particular movieticket (corresponding to <code>productCode</code>
	 * to an invoice corresponding to the provided <code>invoiceCode</code> with
	 * the given number of units
	 */

	public static void addMovieTicketToInvoice(String invoiceCode, String productCode, int quantity) {}

	/*
	 * 13. Adds a particular seasonpass (corresponding to <code>productCode</code>
	 * to an invoice corresponding to the provided <code>invoiceCode</code> with
	 * the given begin/end dates
	 */
	public static void addSeasonPassToInvoice(String invoiceCode, String productCode, int quantity) {}

     /**
     * 14. Adds a particular ParkingPass (corresponding to <code>productCode</code> to an 
     * invoice corresponding to the provided <code>invoiceCode</code> with the given
     * number of quantity.
     * NOTE: ticketCode may be null
     */
    public static void addParkingPassToInvoice(String invoiceCode, String productCode, int quantity, String ticketCode) {}
	
    /**
     * 15. Adds a particular refreshment (corresponding to <code>productCode</code> to an 
     * invoice corresponding to the provided <code>invoiceCode</code> with the given
     * number of quantity. 
     */
    public static void addRefreshmentToInvoice(String invoiceCode, String productCode, int quantity) {}

}
