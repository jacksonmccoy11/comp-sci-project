package com.ceg.ext;

import unl.cse.*;
import unl.cse.Products.*;

import java.sql.*;
import java.util.ArrayList;

public class DatabaseData {
    Connection conn = DatabaseInfo.getConnection();
    PreparedStatement ps = null;

    public ArrayList<Person> getPersons() {
        ArrayList<Person> persons = new ArrayList<Person>();

        try {
            String sql = "SELECT * FROM Persons;";
            ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();

            while(rs.next()) {

                //Gets all fields in Persons table
                String personCode = rs.getString("PersonCode");
                String firstName = rs.getString("FirstName");
                String lastName = rs.getString("LastName");
                String street = rs.getString("Street");
                String city = rs.getString("City");
                String state = rs.getString("State");
                int zip = rs.getInt("Zip");
                String country = rs.getString("Country");

                //Creates address object for this incremented Person
                Address address = new Address(street, city, state, zip, country);

                //Gets all emails for this person
                sql = "SELECT * FROM Emails e WHERE e.PersonCode LIKE ?;";
                ps = conn.prepareStatement(sql);
                ps.setString(1, personCode);
                ResultSet rsEmails = ps.executeQuery(sql);

                String[] emailArray = new String[10];
                int index = 0;
                while(rsEmails.next()) {
                    emailArray[index] = rsEmails.getString("Email");
                    index++;
                }

                Person person = new Person(personCode, firstName, lastName, address, emailArray);
                persons.add(person);
            }
        }
        catch(SQLException e) {
            System.out.println(e);
        }

        return persons;
    }

    public ArrayList<Customer> getCustomers(ArrayList<Person> persons) {
        ArrayList<Customer> customers = new ArrayList<Customer>();

        try {
            String sql = "SELECT * FROM Customers;";
            ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();

            while(rs.next()) {
                String customerCode = rs.getString("CustomerCode");
                String type = rs.getString("CustomerType");
                String personCode = rs.getString("PersonCode");
                String name = rs.getString("CustomerName");
                String street = rs.getString("Street");
                String city = rs.getString("City");
                String state = rs.getString("State");
                int zip = rs.getInt("Zip");
                String country = rs.getString("Country");

                Address address = new Address(street, city, state, zip, country);
                Person person = null;

                for(Person p: persons) {
                    if(p.getPersonCode().equals(personCode)) {
                        person = p;
                    }
                }

                Customer customer = new Customer(customerCode, type, person, name, address);
                customers.add(customer);
            }

        }
        catch(SQLException e) {
            System.out.println(e);
        }

        return customers;
    }

    public ArrayList<Product> getProductsFromDb() {
        ArrayList<Product> products = new ArrayList<Product>();

        try {
            String sql = "SELECT * FROM Products;";
            ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();

            while(rs.next()) {
                Product product = null;
                String productCode = rs.getString("ProductCode");
                String type = rs.getString("ProductType");
                double cost = rs.getDouble("Cost");

                switch(type) {
                    case "M":
                        String movieName = rs.getString("ProductName");
                        String dateTime = rs.getString("MovieDateTime");
                        String address = rs.getString("Address");
                        String screenNo = rs.getString("ScreenNumber");
                        product = new MovieTickets(productCode, dateTime, movieName, address, screenNo, cost);
                        break;
                    case "P":
                        product = new Parking(productCode, cost);
                        break;
                    case "R":
                        String name = rs.getString("ProductName");
                        product = new Refreshments(productCode, name, cost);
                        break;
                    case "S":
                        String sName = rs.getString("ProductName");
                        String startDate = rs.getString("StartDate");
                        String endDate = rs.getString("EndDate");
                        product = new Season(productCode, sName, startDate, endDate, cost);
                        break;
                }
                products.add(product);
            }
        }
        catch(SQLException e) {
            System.out.println(e);
        }

        return products;
    }

    public ArrayList<Invoice> getInvoices(ArrayList<Person> persons, ArrayList<Customer> customers, ArrayList<Product> products) {
        ArrayList<Invoice> invoices = new ArrayList<Invoice>();

        try {
            String sql = "SELECT * FROM Products;";
            ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();

            while(rs.next()) {
                String invoiceCode = rs.getString("InvoiceCode");

                //Finds customer with customerCode
                String customerCode = rs.getString("CustomerCode");
                Customer customer = null;
                for(Customer c: customers) {
                    if(c.getCustomerCode().equals(customerCode)) {
                        customer = c;
                        break;
                    }
                }

                //Finds salesperson with personCode
                String salesPersonCode = rs.getString("SalesPersonCode");
                Person person = null;
                for(Person p: persons) {
                    if(p.getPersonCode().equals(salesPersonCode)) {
                        person = p;
                        break;
                    }
                }

                String invoiceDate = rs.getString("InvoiceDate");

                ArrayList<Product> invoiceProducts = new ArrayList<Product>();
                int numOfProducts[] = new int[10];
                String linkedProductCode[] = new String[10];
                int index = 0;

                //Sql for finding all products for the invoice code
                sql = "SELECT * FROM InvoiceProducts ip WHERE ip.InvoiceCode LIKE ?;";
                ps = conn.prepareStatement(sql);
                ps.setString(1, invoiceCode);
                ResultSet rsProducts = ps.executeQuery(sql);

                //Gets all the products mapped to this invoice
                while(rsProducts.next()) {
                    String productCode = rsProducts.getString("ProductCode");
                    numOfProducts[index] = rsProducts.getInt("NumOfProducts");
                    index++;

                    for(Product p: products) {
                        if(p.getProductCode().equals(productCode)) {
                            invoiceProducts.add(p);
                        }
                    }
                }

                InvoiceReport invoiceReport = new InvoiceReport(invoiceCode, customer, person, invoiceDate, invoiceProducts, numOfProducts, linkedProductCode);
                invoices.add(invoiceReport);
            }
        }
        catch(SQLException e) {
            System.out.println(e);
        }

        return invoices;
    }
}
